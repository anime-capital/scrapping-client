require "kirinnee_core"
require_relative './class_library/helper'
require_relative './class_library/horrible_sub_client'
require_relative './class_library/azure_client'
require_relative './class_library/logger'
require_relative './class_library/downloader'
require_relative './class_library/canime_client'

organization = ENV["ORGANIZATION"]
project = ENV["PROJECT"]
secret = ENV["PAT"]
api_secret = ENV["SECRET"]
link = ENV["LINK"]
host = ENV["HOST"]
email = ENV["USER_EMAIL"]
name = ENV["USER_NAME"]

$azure = AzureClient.new(organization, project, secret)
$horrible = HorribleSubClient.new
$l = CustomLogger.new(link == "" ? nil : link)
$h = Helper.new
$downloader = Downloader.new(name, email)
$api = CanimeClient.new(organization, project, host, api_secret)

$l.log "Singletons loaded!"

$l.log "Starting execution of scripts..."
