require_relative './singletons'


p "Begin Anime Scrapping"

# @param [Array<Anime>] array
def threaded_run(array)
	return if array.length == 0
	anime = array.pop
	$l.log "Warming Thread..."
	sleep 5
	$l.log "==============================================================="
	$l.log "Checking if **#{anime.name}** episode **#{anime.episode}** is already begin downloaded..."
	state_value = $api.get_state(anime.name, anime.episode)
	$l.log "It is of state **#{state_value}**"
	if state_value == 0
		$l.log "Begin downloading **#{anime.name}** of Episode **#{anime.episode}**!"
		$l.log "Telling API server this runner is starting to download **#{anime.name}** episode **#{anime.episode}**..."
		val = $api.set_state anime.name, anime.episode, 1
		if val
			$l.log "Successfully notified server!"
			success, done = $downloader.download_anime anime
		else
			$l.log "Failed to notify server, skipping this episode..."
			success = 2
		end
	else
		$l.log "Another runner is downloading it, or it has been downloaded, skipping..."
		success = 3
	end
	if success == 0
		$l.log "Succeeded downloading **#{anime.name}** episode **#{anime.episode}**  ^^"
		$l.log "Attempting to update server api..."
		val = $api.update_episode done.name, done.episode
		if val.code != 200
			$l.log "Failed posting to server api with code #{val.code}"
			$l.log "Error is as follows:"
			$l.log val.content
		else
			$l.log "Successfully updated to server api!"
		end
	elsif success == 1
		$l.log "**#{anime.name}** episode **#{anime.episode}** has already been downloaded!"
		val = $api.set_state anime.name, anime.episode, 1
		if val
			$l.log "Successfully notified server!"
		else
			$l.log "Failed to notify the server!"
		end
	elsif success == 2
		$l.log "Failed downloading **#{anime.name}** episode **#{anime.episode}** :<"
		$l.log "Updating API server the runner has failed downloading.."
		val = $api.set_state anime.name, anime.episode, 0
		if val
			$l.log "Successfully notified server!"
		else
			$l.log "Failed to notify the server!"
		end
	elsif success == 3
		$l.log "**#{anime.name}** episode **#{anime.episode}** is either downloading, or has been downloaded!"
		$l.log "Updated API server the runner is already running"
		val = $api.set_state anime.name, anime.episode, 1
		if val
			$l.log "Successfully notified server!"
		else
			$l.log "Failed to notify the server!"
		end
	end
	$l.log "==============================================================="
	$l.log "Remaining anime to download:"
	$l.log "[" + array.map {|x| "**#{x.episode_name}**"}.join(" , ") + "]"
	$l.log "==============================================================="
	threaded_run array
end

while true
	$l.log "Obtaining Anime from HorribleSubs itself..."
	anime = $horrible.run.select {|x| x.episode < 48}
	$l.log "Obtained a total of #{anime.length} animes!"
	$l.log "Preparing to download..."

	t1 = Thread.new {threaded_run anime}
	t1.join

	$l.log "Completed downloading all batches!"
	$l.log "Scrapper will now go into idle mode and will resume work in 120 min"
	sleep 60
	119.times do |x|
		$l.log "Scrapper will resume work in #{120 - x} min"
		sleep 60
	end
	$l.log "Done idling OwO"
	$l.log "Will begin next cycle shortly..."
	sleep 5
end


