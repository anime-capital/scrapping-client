FROM ruby:2.6
RUN apt-get update

# Install FFMPEG
RUN apt-get install -y ffmpeg

# Install NPM
RUN curl -sL https://deb.nodesource.com/setup_11.x -o nodesource_setup.sh
RUN bash nodesource_setup.sh
RUN apt-get install -y nodejs

# Install Yarn
RUN npm i -g yarn

# install web client
RUN yarn global add webtorrent-hybrid
RUN yarn global add webtorrent-cli

# install LFS
RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:git-core/ppa
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash
RUN apt-get install -y git-lfs
RUN git lfs install

# Config to not download LFS
RUN git lfs install --skip-smudge

# setup space
COPY . .
RUN bundle install

# Entry point
CMD ruby scrape.rb