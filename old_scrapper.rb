require_relative './singletons'

delay = ENV["DELAY"].to_i
p delay
sleep delay

$l.log "Begin Anime Scrapping"
# @param [Anime] anime
def threaded_run(anime)
	$l.log "Warming Thread..."
	sleep 5
	$l.log "==============================================================="
	$l.log "Checking if **#{anime.name}** episode **#{anime.episode}** is already begin downloaded..."
	state_value = $api.get_state(anime.name, anime.episode)
	$l.log "It is of state **#{state_value}**"
	done = nil
	if state_value == 0
		$l.log "Begin downloading **#{anime.name}** of Episode **#{anime.episode}**!"
		$l.log "Telling API server this runner is starting to download **#{anime.name}** episode **#{anime.episode}**..."
		val = $api.set_state anime.name, anime.episode, 1
		if val
			$l.log "Successfully notified server!"
			success, done = $downloader.download_anime anime
		else
			$l.log "Failed to notify server, skipping this episode..."
			success = 2
		end
	else
		$l.log "Another runner is downloading it, or it has been downloaded, skipping..."
		success = 3
	end
	if success == 0
		$l.log "Succeeded downloading **#{anime.name}** episode **#{anime.episode}**  ^^"
		$l.log "Attempting to update server api..."
		val = $api.update_episode_old done.name, done.episode
		if val.code != 200
			$l.log "Failed posting to server api with code #{val.code}"
			$l.log "Error is as follows:"
			$l.log val.content
		else
			$l.log "Successfully updated to server api!"
		end
	elsif success == 1
		$l.log "**#{anime.name}** episode **#{anime.episode}** has already been downloaded!"
		val = $api.set_state anime.name, anime.episode, 1
		if val
			$l.log "Successfully notified server!"
		else
			$l.log "Failed to notify the server!"
		end
	elsif success == 2
		$l.log "Failed downloading **#{anime.name}** episode **#{anime.episode}** :<"
		$l.log "Updating API server the runner has failed downloading.."
		val = $api.set_state anime.name, anime.episode, 0
		if val
			$l.log "Successfully notified server!"
		else
			$l.log "Failed to notify the server!"
		end
	elsif success == 3
		$l.log "**#{anime.name}** episode **#{anime.episode}** is either downloading, or has been downloaded!"
		$l.log "Updated API server the runner is already running"
		val = $api.set_state anime.name, anime.episode, 1
		if val
			$l.log "Successfully notified server!"
		else
			$l.log "Failed to notify the server!"
		end
	end
end


$done = {}

def dl_next(name, ep)
	return if ep == 0
	$l.log "Found **#{name} episode #{ep}** to look for download link "
	anime = $horrible.anime_search name, ep
	if anime.nil?
		$l.log "Anime not found.. :<"
		$done["#{name} #{ep}"] = 0
		sleep 5
	else
		$l.log "Anime data: **#{anime.name}**; **#{anime.episode}**; **#{anime.link}**"
		threaded_run anime
		sleep 5
		$l.log "Downloading next episode!"
		dl_next(name, ep - 1)
	end
end

while true
	$l.log "Looking for anime to download"
	list = $api.get_list 0, 50
	filtered = list.filter {|x| x["episode"] < 48 && x["rating"] > 7}
	download_target = []
	filtered.each do |ani|
		id = ani["id"]
		details = $api.get_anime id
		episode = details["episodes"].min {|a, b| a.number - b.number}
		ep_num = episode["number"]
		ep_name = details["name"]
		state_val = $api.get_state "Full Anime #{ep_name}", 0
		next if state_val == 1
		$l.log "Episode #{ep_num} is the smallest for #{ep_name}"
		if ep_num > 1 && ep_num < 49 && $done["#{ep_name} #{ep_num - 1}"].nil?
			download_target = [ep_name, ep_num - 1]
			break
		end
	end
	if download_target.length == 2
		name = download_target[0]
		ep = download_target[1]
		$api.set_state "Full Anime #{name}", 0, 1
		dl_next name, ep
		$api.set_state "Full Anime #{name}", 0, 0
	else
		$l.log "No anime left...! Idle for 5 min!"
		sleep 600
	end
	sleep 20
end