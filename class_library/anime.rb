require "kirinnee_core"
class Anime

	attr_reader :link, :episode, :name

	# @param [String] name
	def initialize(name, episode, link)
		@name = name.without ['!', '.', '%', '#', '$', '*', '}', '{', '+', '=', '\n', '\r', ';', '/', '?', ':', '@', '=', '&', '<', '>', '(', ')', "'", '"', "~", ",", "|", "[", "]"]
		@episode = episode
		@link = link
	end

	def episode_name
		"#{@name} Episode #{@episode}"
	end

end
