class RepositoryData
	def initialize(pat, remote, folder_name = nil, name = nil)
		front, back = remote.split "@"
		# @type [String]
		@remote = "#{front}:#{pat}@#{back}"
		# @type [String]
		@folder_name = folder_name || remote.split("/").last
		# @type [String]
		@name = name || @folder_name&.replace_all("%20", " ")
	end

	# @return [String]
	def remote
		@remote
	end

	# @return [String]
	def folder_name
		@folder_name
	end

	# @return [String]
	def name
		@name
	end

end