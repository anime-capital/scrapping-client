require "dotenv/load"
require "kirinnee_core"
require "kirin_http"
require "base64"
require "json"
require_relative './repository_data'
require_relative './real_repo_instance'


class AzureClient
	def initialize(org, project, pat)
		@organization = org
		@project = project
		@pat = pat
		@http = KirinHttp::BasicClient.new
	end

	# @param[String] endpoint
	# @param [Symbol] method
	# @param [Object] content
	# @return [Kirin::Response]
	def ping(endpoint, method = :get, content = nil)
		token = Base64.encode64 ":#{@pat}"
		token.replace_all! "\n", ""
		message = KirinHttp::Message.new(endpoint, method, content, {
			"Accept": "application/json",
			"Content-Type": "application/json",
			"Authorization": "Basic #{token}"
		})
		@http.send message
	end

	def create_cloud_repository(repository)
		$l.log "Creating cloud repository with **#{repository}**"
		endpoint = "https://dev.azure.com/#{@organization}/#{@project}/_apis/git/repositories?api-version=5.0"
		value = ping endpoint, :post, {"name": repository}.to_json
		if value.code == 201
			remote = JSON.parse(value.content)["remoteUrl"]
			$l.log "Created remote, value: #{remote}"
			RepositoryData.new(@pat, remote)
		else
			$l.log "Received code: #{value.code}"
			$l.log value.content
			raise StandardError.new("Failed to create cloud repository...")
		end
	end

	# @return [Array<RepositoryData>]
	def get_repositories
		endpoint = "https://dev.azure.com/#{@organization}/#{@project}/_apis/git/repositories?api-version=5.0"
		value = ping endpoint
		if value.code == 200
			v = JSON.parse(value.content).values[0]
			v.map {|x| RepositoryData.new(@pat, x["remoteUrl"])}
		else
			$l.log "Failed to obtained repositories with a code of #{value.code}"
			$l.log value.content
			raise StandardError.new("Failed to get repositories from **#{@project}** of organization **#{@organization}**")
		end

	end

	# @return boolean
	def exist_repository(name)
		$l.log "Checking if **#{name}** repository exist..."
		names = get_repositories.map(&:name)
		exist = names.select {|x| x == name}.length > 0
		$l.log "Repository **#{name}** #{exist ? "exist" : "does not exist"}!"
		exist
	end

	# @return [RepositoryData]
	def get_repository(name)
		$l.log "Attempting to look for **#{name}** repository"
		get_repositories.select {|x| x.name == name}[0]
	end

end

