require "securerandom"

class UploadDataFactory

	def initialize(repo)
		@repo = repo
	end

	# @param [Anime] anime
	# @param [String] type
	def create(anime, type)
		link = if type === "480"
				   anime.link_480
			   else
				   type === "720" ? anime.link_720 : anime.link_1080
			   end
		UploadData.new(anime.entry_name, link, @repo, type, anime.episode_name)
	end

end

class UploadData
	# @param [String] name the redis cache entry name
	# @param [RepositoryData] repository azure client repository
	# @param [String] branch the quality, also the branch
	# @param [String] episode_name the episode name
	def initialize(name, link, repository, branch, episode_name)
		@uuid = SecureRandom.uuid
		@name = name
		@link = link
		@repository = repository
		@branch = branch
		@episode_name = episode_name
	end

	# @return [String] uuid
	def uuid
		@uuid
	end

	# @return [String] name
	def name
		@name
	end

	# @return [String] link
	def link
		@link
	end

	# @return [RepositoryData]
	def repository
		@repository
	end

	# @return [String]
	def branch
		@branch
	end

	# @return [String]
	def episode_name
		@episode_name
	end

end