require "kirinnee_core"
require "kirin_http"
require "nokogiri"
require "json"
require "open-uri"
require_relative './anime'

class HorribleSubClient
	def initialize
		@http = KirinHttp::BasicClient.new
	end

	# @return [Array<Anime>]
	def run
		$l.log "Pulling first list of anime from Horriblesubs..."
		anime1 = animes base_link
		$l.log "Done! A total of #{anime1.length} has been pulled for first batch!"
		$l.log "Pulling second list of anime from Horriblesubs..."
		anime2 = animes additional_link 0
		$l.log "Done! A total of #{anime2.length} has been pulled for second batch!"
		$l.log "Pulling third list of anime from Horriblesubs..."
		anime3 = animes additional_link 1
		$l.log "Done! A total of #{anime3.length} has been pulled for third batch!"
		anime1 | anime2 | anime3
	end

	# @return [Array<Anime>]
	def animes(link, limit = -1)
		info = obtain_basic_info_from link
		info = info.take limit unless limit == -1
		get_anime_from info
	end


	# @param [Integer] id
	# @return [String]
	def additional_link(id)
		"https://horriblesubs.info/api.php?method=getlatest&nextid=#{id}&_=#{stamp}"
	end

	# @return [String]
	def base_link
		"https://horriblesubs.info/api.php?method=getlatest&_=#{stamp}"
	end

	# @param [String] search_key the anime name
	# @param [Integer] x the episode number
	def anime_search(search_key, x)
		info = obtain_basic_info_from "https://horriblesubs.info/api.php?method=search&value=#{search_key.replace_all " ", "+"}&_=#{stamp}"
		return nil if info.length == 0
		a_i = info[0]
		v = a_i[:ep]
		pages = (v / 12.0).ceil
		ori_link = a_i[:link].split("#").omit(1).join("#") + "##{x}"
		link = nil
		pages.times do |y|
			p link
			break unless link.nil?
			link = obtain_link ori_link, y
		end
		if link.nil?
			nil
		else
			Anime.new(a_i[:name], x, link)
		end
	end

	private

	def get_anime_from(info)
		info.map do |x|
			link = obtain_link x[:link]
			Anime.new(x[:name], x[:ep], link)
		end
	end

	def obtain_basic_info_from(endpoint)
		result = ping endpoint
		parsed = Nokogiri.HTML result
		parsed.search("li").map do |x|
			element = x.at("a")
			link = element["href"]
			element.at(".latest-releases-date").remove
			element.at(".latest-releases-res").remove
			episode = element.at("strong")
			ep = episode.text.to_i
			episode.remove
			text = element.content.omit(3).strip
			{link: link, ep: ep, name: text}
		end
	end

	def obtain_anime_id(relative)
		r = open "https://horriblesubs.info#{relative}"
		result = Nokogiri.HTML r.read
		y, _ = result.search("script").map(&:content).where { |x| x.strip.start_with? "var hs_showid" }[0].match(/([0-9]+)/).captures
		y
	end

	def obtain_link(relative, n = 0)
		id = obtain_anime_id relative
		ep = relative.split("#").last.to_i
		endpoint = "https://horriblesubs.info/api.php?method=getshows&type=show&nextid=#{n}&showid=#{id}"
		r = ping endpoint
		parsed = Nokogiri.HTML r
		obtain_href parsed, ep, "1080p"
	end

	def obtain_href(parsed, ep, p)
		ep = "0" + ep.to_s if ep < 10
		link = parsed&.at("##{ep}-#{p}")&.at("span.dl-type.hs-magnet-link")&.at("a")
		link.nil? ? nil : link['href']
	end

	def stamp
		Time.now.to_i
	end

	def ping(endpoint)
		request = KirinHttp::Message.new(endpoint, :get, nil, {
				"accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
				"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
		})
		response = @http.send(request)
		response.content
	end
end



