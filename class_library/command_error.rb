class CommandError < StandardError
	def initialize(command, error)
		super("Error occurred while executing #{command}:\n #{error}")
	end
end