require "securerandom"
class Torrent
  def initialize(this)
    @this = this
  end

  # @param [String] magnet magnet link
  # @param [String] name name of the file, excluding extension
  # @param [String] target of folder to move to
  def download(magnet, name, target)
    $l.log "Downloading..."
    uuid = SecureRandom.uuid
    # create scope
    run "mkdir #{uuid.wrap}"
    begin
      run "chmod o+x #{uuid.wrap}"
    rescue StandardError => e
      puts e
    end
    # download into controlled scoped environment
    run "cd #{uuid.wrap} && webtorrent download #{magnet.wrap}"
    # rename the god damn file
    file = Dir.glob($h.combine(@this, uuid, "**/*.mkv"))[0]
    name = $h.rename file, name
    $l.log "Download Complete, moving..."
    # more file
    FileUtils.mkdir_p $h.combine(@this, target)
    end_target = $h.combine @this, target
    FileUtils.mv name, end_target
    run "rm -rf #{uuid.wrap}"
    $l.log "Moved to #{target}"
  end

  private

  def run(command)
    $h.execute "cd #{@this.wrap} && #{command}"
  end
end