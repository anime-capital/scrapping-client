require "securerandom"
require_relative './upload'

class Downloader
  def initialize(user, email)
    @user = user
    @email = email
  end

  # @param [Anime] anime
  # @return [Boolean, Anime]
  def download_anime(anime)
    folder_name = SecureRandom.uuid
    failed = false
    download = false
    begin
      FileUtils.mkdir_p folder_name
      uploader = Uploader.new(folder_name, @user, @email)
      download = uploader.upload anime.name, anime.episode, anime.link
    rescue StandardError => e
      $l.log "The task of uploading **#{anime.name}** has failed, logs as follows: "
      $l.log "===================================================================="
      $l.log e.message
      $l.log "===================================================================="
      failed = true
    end
    begin
      $h.execute "rm -rf #{folder_name}"
    rescue StandardError => e
      $l.log "Failed removing folder??"
      $l.log e.message
    end
    response = if failed
                 2
               else
                 download ? 0 : 1
               end
    [response, anime]
  end
end