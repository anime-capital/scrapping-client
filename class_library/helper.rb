require "pathname"
require "fileutils"
require "open3"
require_relative './command_error'

class Helper
	def combine(*args)
		Pathname.new(File.join(args)).cleanpath.to_s
	end

	def rename(file, target)
		ext = File.extname file
		folder = File.dirname file
		output = combine(folder, "#{target}#{ext}")
		FileUtils.mv file, output
		output
	end

	def execute(command)
		out, err, status = Open3.capture3(command)
		if status.success?
			File.open('logs.log', 'a') {|f| f.write(out)}
			out
		else
			raise CommandError.new(command, err)
		end
	end

end

class String
	def wrap
		'"' + self + '"'
	end
end