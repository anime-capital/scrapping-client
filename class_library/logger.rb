require "json"
require "kirin_http"
class CustomLogger

	def initialize(endpoint)
		@http = KirinHttp::BasicClient.new
		@endpoint = endpoint
	end

	def log(value)
		s = to_s value
		print Time.now.utc.to_s + " " + s + "\n"

		unless @endpoint.nil?
			begin
				endpoint = @endpoint
				content = {
					"content": s
				}
				message = KirinHttp::Message.new(endpoint, :post, content.to_json)
				@http.send message
			rescue StandardError => e
				p e
			end
		end
	end

	def to_s(value)
		case value
			when String
				val = value
			when Integer
				val = value.to_s
			when Float
				val = value.to_s
			else
				val = value.to_json
		end
		val
	end
end