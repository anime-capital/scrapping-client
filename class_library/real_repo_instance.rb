class RealRepoInstance

  def initialize(prepend, remote_url, user, email)
    @prepend = prepend
    @remote = remote_url
    @folder_name = @remote.split("/").last
    @name = @folder_name.replace_all("%20", " ")
    @user = user
    @email = email
  end

  # @return [String]
  def folder
    @folder_name
  end

  def create
    $l.log "Creating repository..."
    shell "mkdir #{@folder_name.wrap}"
    begin
      shell "chmod o+x #{@folder_name.wrap}"
    rescue StandardError => e
      puts e
    end
    run "git init"
    run "git remote add origin #{@remote}"
    set_user "#{@user}"
    set_email "#{@email}"
    content = "##{@name}'s Repository for Streaming"
    run "echo #{content.wrap} > README.md"
    add "."
    commit "Create folder for #{@name}"
    push "master"
    $l.log "Created repository"
  end

  def clone
    $l.log "Cloning #{@remote}"
    # clone repository
    shell "git clone #{@remote.wrap}"
    begin
      # provide permission to enter folder
      shell "chmod o+x #{@folder_name.wrap}"
    rescue StandardError => e
      puts e
    end
  end

  def checkout(branch)
    run "git checkout #{branch}"
  end

  def add(files)
    run "git add #{files}"
  end

  def lfs_track(glob)
    run "git lfs track #{glob}"
  end

  def set_user(name)
    run "git config user.name #{name.wrap}"
  end

  def set_email(email)
    run "git config user.email #{email.wrap}"
  end

  def commit(message)
    run "git commit -m \"#{message}\""
  end

  def push(branch)
    run "git push origin #{branch}"
  end

  def exist_file(file_name)
    File.file? ($h.combine(@prepend, @folder_name, file_name))
  end

  def delete
    $h.execute "cd #{@prepend.wrap} && rm -rf #{@folder_name.wrap}"
  end


  private

  def shell(command)
    $h.execute "cd #{@prepend.wrap} && #{command}"
  end

  def run(command)
    $h.execute "cd #{merged.wrap} && #{command}"
  end

  def merged
    $h.combine @prepend, @folder_name
  end

end