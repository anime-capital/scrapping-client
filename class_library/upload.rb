require "fileutils"
require "pathname"
require_relative './torrent'
require_relative './ffmpeg'
require_relative './real_repo_instance'

class Uploader

  def initialize(folder, user, email)
    @this = folder
    @user = user
    @email = email
  end

  def upload(anime, episode, magnet)
    download = false
    # Check if file already exist via new endpoint
    ep_exist = $api.exist_episode anime, episode
    $l.log "**#{anime}** episode **#{episode}** #{ep_exist ? "exist" : "does not exist"}"
    return download if ep_exist
    exist = $azure.exist_repository anime
    if exist
      repository = $azure.get_repository anime
      git = RealRepoInstance.new(@this, repository.remote, @user, @email)
      git.clone
    else
      repository = $azure.create_cloud_repository anime
      git = RealRepoInstance.new(@this, repository.remote, @user, @email)
      git.create
    end
    target_folder = $h.combine @this, git.folder, "Episode #{episode}"
    $l.log "Checking if folder for **#{target_folder}** exist..."
    f_exist = Dir.exists? target_folder
    $l.log "It #{f_exist ? "exist" : "does not exist"}!"
    $l.log "Checking if file exist..."
    file_exist = File.exists?($h.combine target_folder, "output.mpd")
    $l.log "It #{file_exist ? "exist" : "does not exist"}!"
    unless f_exist && file_exist
      $l.log "Begin torrent-ing..."
      FileUtils.mkdir_p target_folder
      download_file("output", target_folder, magnet)

      $l.log "Tracking lfs..."
      git.lfs_track "**/*.*"

      push(git, anime, "mkv")
      push(git, anime, "mp4")
      push(git, anime)

      $l.log "Done!"
      download = true
    end
    $l.log "Deleting temp folder..."
    git.delete
    $l.log "Deleted!"
    download
  end

  private

  def push(git, anime, extension = nil)

    $l.log "Staging mp4 to git..."

    if extension.nil?
      git.add "."
      extension = "all"
    else
      begin
        git.add "**/*.#{extension}"
      rescue StandardError => e
        $l.log e
      end
      begin
        git.add "*.#{extension}"
      rescue StandardError => e
        $l.log e
      end
    end
    git.set_user "#{@user}"
    $l.log "added #{@user} as user"
    git.set_email "#{@email}"
    $l.log "added #{@email} as email"

    $l.log "Committing files.."
    git.commit "Uploaded #{extension} **#{anime}**"

    $l.log "Pushing to master branch..."
    git.push "master"
  end

  def download_file(name, folder, link)
    torrent = Torrent.new(".")
    torrent.download link, name, folder
    ffmpeg = FFMPEG.new(folder)
    ffmpeg.mkv_to_mp4 name
    ffmpeg.dash name, "output"
  end

end