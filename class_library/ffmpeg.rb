require "fileutils"

class FFMPEG

	def initialize(context)
		@folder = context
	end

	# @param [String] file_name name without extension
	def mkv_to_mp4(file_name)
		$l.log "Begin conversion from mkv to mp4 for #{file_name}"
		mkv = (file_name + ".mkv").wrap
		mp4 = (file_name + ".mp4").wrap
		run "ffmpeg -i #{mkv} -vf subtitles=#{mkv} -preset ultrafast -acodec copy #{mp4}"
		FileUtils.chmod 0777, Dir.glob($h.combine(@folder, "**/*.*"))
		$l.log "Conversion Completed"
	end

	# @param [String] file_name name without extension
	# @param [String] mpd mpd file name WITHOUT extension
	def dash(file_name, mpd)
		file_name = (file_name + ".mp4").wrap
		$l.log "Converting to streaming format..."
		input = "ffmpeg -i #{file_name} -preset ultrafast "
		map = "-map 0:v:0 -map 0:a:0 -map 0:v:0 -map 0:v:0 -map 0:v:0 "
		v360 = "-filter:v:1 \"scale=-2:360\" -profile:v:1 baseline "
		v480 = "-filter:v:2 \"scale=-2:480\" -profile:v:2 baseline "
		v720 = "-filter:v:3 \"scale=-2:720\" -profile:v:3 baseline "
		v1080 = "-filter:v:4 \"scale=-2:1080\" -profile:v:4 baseline "
		config = "-min_seg_duration 3 -use_timeline 1 -use_template 1 "
		execution = "-f dash -adaptation_sets \"id=0,streams=v id=1,streams=a\" "
		output = "#{mpd}.mpd"
		run(input + map + v360 + v480 + v720 + v1080 + config + execution + output)
		$l.log "Stream conversion completed!"
		FileUtils.chmod 0777, Dir.glob($h.combine(@folder, "**/*.*"))
	end

	private

		def run(command)
			$h.execute "#{switch} #{command}"
		end

		def switch
			"cd #{@folder.wrap} &&"
		end
end