require "kirin_http"
require "json"
require "digest"
require "securerandom"

class CanimeClient
	def initialize(organization, project, host, secret)
		@http = KirinHttp::BasicClient.new
		@org = organization
		@project = project
		@host = host
		@secret = secret
	end

	def update
		uuid = SecureRandom.uuid
		endpoint = "#{@host}/api/Anime?payload=#{uuid}"
		data = uuid + ";" + @secret
		hex = Digest::SHA256.hexdigest data
		message = KirinHttp::Message.new(endpoint, :patch, nil, {"super-secret": hex})
		@http.send message
	end

	# @return [Array]
	def get_list(page, amount)
		endpoint = "#{@host}/api/Anime?page=#{page}&amount=#{amount}"
		message = KirinHttp::Message.new(endpoint)
		v = @http.send message
		if v.code == 200
			JSON.parse(v.content)
		else
			raise StandardError.new("Failed: #{v.content}")
		end
	end

	# @param [String] name
	# @param [Integer] episode
	# @param [Integer] state
	def set_state(name, episode, state)
		ep = name.replace_all " ", "%20"
		endpoint = "#{@host}/api/Anime/status/#{ep}/#{episode}/#{state}"
		payload = "#{name}_#{episode}_#{state}"
		data = payload + ";" + @secret
		hex = Digest::SHA256.hexdigest data
		message = KirinHttp::Message.new(endpoint, :post, nil, {"super-secret": hex})
		v = @http.send message
		v.code == 200

	end

	# @param [String] name
	# @param [Integer] episode
	def get_state(name, episode)
		ep = name.replace_all " ", "%20"
		endpoint = "#{@host}/api/Anime/status/#{ep}/#{episode}"
		payload = "#{name}_#{episode}"
		data = payload + ";" + @secret
		hex = Digest::SHA256.hexdigest data
		message = KirinHttp::Message.new(endpoint, :get, nil, {"super-secret": hex})
		v = @http.send message
		if v.code == 200
			v.content.to_i
		else
			raise StandardError.new("Failed: #{v.content}")
		end
	end

	def get_anime(id)
		endpoint = "#{@host}/api/Anime/#{id}"
		message = KirinHttp::Message.new(endpoint)
		v = @http.send message
		if v.code == 200
			JSON.parse(v.content)
		else
			raise StandardError.new("Failed: #{v.content}")
		end
	end

	# @param [String] name
	def exist_episode(name, episode)
		v = name.replace_all " ", "%20"
		endpoint = "#{@host}/api/Video/#{@org}/#{@project}/#{v}/master/Episode%20#{episode}/output.mpd"
		message = KirinHttp::Message.new(endpoint, :head)
		v = @http.send message
		v.code == 200
	end

	def update_episode(name, episode)
		content = {
			"AnimeName": name,
			"EpisodeNumber": episode,
			"Organization": @org,
			"Project": @project
		}
		data = content.to_json + ";" + @secret
		hex = Digest::SHA256.hexdigest data
		message = KirinHttp::Message.new("#{@host}/api/anime", :post, content.to_json, {
			"super-secret": hex,
			"Content-Type": "application/json"
		})
		@http.send message
	end

	def update_episode_old(name, episode)
		content = {
			"AnimeName": name,
			"EpisodeNumber": episode,
			"Organization": @org,
			"Project": @project
		}
		data = content.to_json + ";" + @secret
		hex = Digest::SHA256.hexdigest data
		message = KirinHttp::Message.new("#{@host}/api/anime/old", :post, content.to_json, {
			"super-secret": hex,
			"Content-Type": "application/json"
		})
		@http.send message
	end

end